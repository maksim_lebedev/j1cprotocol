package cl.itf.j1c.protocol;

/**
 * @author sniffl
 */
public class AddBase {
    
    private final String path;
    private final String user;
    private final String pass;
    private final boolean isServer;

    public AddBase(String path, String user, String pass, boolean isServer) {
        this.path = path;
        this.user = user;
        this.pass = pass;
        this.isServer = isServer;
    }

    public String getPath() {
        return path;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public boolean isIsServer() {
        return isServer;
    }
}
