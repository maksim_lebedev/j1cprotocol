package cl.itf.j1c.protocol;

/**
 * @author lebedev-m
 */
public class Report {

    //-----------------------------------------------------
    private boolean mark;

    public boolean isMark() {
        return mark;
    }

    public void setMark(boolean mark) {
        this.mark = mark;
    }
    
    //-------------------------------------------
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //-------------------------------------------
    private long date1;

    public long getDate1() {
        return date1;
    }

    public void setDate1(long date1) {
        this.date1 = date1;
    }

    //-------------------------------------------
    private long date2;

    public long getDate2() {
        return date2;
    }

    public void setDate2(long date2) {
        this.date2 = date2;
    }
    
    //-------------------------------------------
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    //-------------------------------------------
    private int type;

    public String getType() {
        return String.valueOf(type);
    }

    public void setType(int type) {
        this.type = type;
    }
}
