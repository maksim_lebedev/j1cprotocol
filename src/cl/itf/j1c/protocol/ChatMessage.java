package cl.itf.j1c.protocol;

import java.io.Serializable;

/*
 * This class defines the different type of messages that will be exchanged between the
 * Clients and the Server. 
 * When talking from a Java Client to a Java Server a lot easier to pass Java objects, no 
 * need to count bytes or to wait for a line feed at the end of the frame
 */
public class ChatMessage implements Serializable {

    protected static final long serialVersionUID = 1112122201L;

    // The different types of message sent by the Client
    // MESSAGE an ordinary message
    // LOGOUT to disconnect from the Server
    public static final int ACCEPTED = 0;
    public static final int MESSAGE = 1;
    public static final int RESULT = 2;
    public static final int LOGOUT = 3;
    public static final int ERROR = 4;

    public static final String delim = "@";

    public static final String HANDSHAKE = "handshake";
    public static final String GETFIRMIND = "getfirm";
    public static final String REPGENIND = "repgen";

    private final int type;
    private final String message;

    // constructor
    public ChatMessage(int type, String message) {
        this.type = type;
        this.message = message;
    }

    // getters
    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
