package cl.itf.j1c.protocol;

import java.util.Arrays;
import java.util.List;

/**
 * @author sniffl
 */
enum StReport {
    
    OSV("Оборотно Сальдовая Ведомость"), 
    OSVCONT("Оборотно Сальдовая Ведомость По Счету"),
    ACCBALCARD("Карточка Счета"),
    ACCANALYSIS("Анализ Счета"),
    ACCANALYSUBCONTO("Анализ Счета По Субконто"),
    ANALYSUBCONTO("Анализ Субконто"),
    REPTRANSACT("Отчет По Проводкам");
    
    private final String name;
    
    StReport(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static List<String> getRepList() {
        return Arrays.asList("Оборотно Сальдовая Ведомость",
                "Оборотно Сальдовая Ведомость По Счету",
                "Карточка Счета",
                "Анализ Счета",
                "Анализ Счета По Субконто",
                "Анализ Субконто",
                "Отчет По Проводкам");
    }
}
