package cl.itf.j1c.protocol;

import java.util.List;

/**
 * @author sniffl
 */
public class RepGen {
    
    private final String savepath;
    private final String company;
    private final String label;
    private final List<Report> reports;

    public RepGen(String savepath, String company, String label,
                  List<Report> reports) {
        this.savepath = savepath;
        this.company = company;
        this.label = label;
        this.reports = reports;
    }

    public String getSavepath() {
        return savepath;
    }

    public String getCompany() {
        return company;
    }

    public String getLabel() {
        return label;
    }

    public List<Report> getReports() {
        return reports;
    }
    
    public StReport getReportType(String name) {

        if (StReport.OSV.getName().equals(name)) {
            return StReport.OSV;
        }
        
        if (StReport.OSVCONT.getName().equals(name)) {
            return StReport.OSVCONT;
        }
        
        if (StReport.ACCBALCARD.getName().equals(name)) {
            return StReport.ACCBALCARD;
        }
        
        if (StReport.ACCANALYSIS.getName().equals(name)) {
            return StReport.ACCANALYSIS;
        }
        
        if (StReport.ACCANALYSUBCONTO.getName().equals(name)) {
            return StReport.ACCANALYSUBCONTO;
        }
        
        if (StReport.ANALYSUBCONTO.getName().equals(name)) {
            return StReport.ANALYSUBCONTO;
        }
        
        if (StReport.REPTRANSACT.getName().equals(name)) {
            return StReport.REPTRANSACT;
        }
        
        return null;
    }
}
